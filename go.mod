module github.com/kamilsk/stash

go 1.11

require (
	github.com/stretchr/testify v1.4.0
	go.octolab.org/toolkit/cli v0.0.6
)
